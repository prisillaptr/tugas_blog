<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BlogTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('blog')->insert([
            'title' => '7 Hal yang belum terpecahkan di Drama Psycho But Its Okay',
            'content' => 'Drama Psycho but Its Okay yang diperankan oleh Kim Soohyun dan Seo Yeaji yang sudah tamat pada pekan lalu ini maish meninggalkan misteri yang belum dipecahkan.',
            'category' => 'Drama/Film'
        ]);
    }
}
